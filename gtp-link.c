/* Command line utility to create GTP link and system cmd to set
  the interface details */

/* (C) 2014 by sysmocom - s.f.m.c. GmbH
 * (C) 2016 by Pablo Neira Ayuso <pablo@netfilter.org>
 * (C) 2020 by ShunmugaPriya Ramanathan <sxr173131@utdallas.edu>
 *
 *
 * All Rights Reserved
 * This program uses the free software developed by Pablo Neira
 * for the GPRS link creation and system command settings for the
 * LTE SPGW application developed by OpenAirInterface team.
 *
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * For more information about the OpenAirInterface (OAI) Software Alliance:
 *      contact@openairinterface.org
 */
 




#include <stdio.h>
#include <stdlib.h>
#include <libconfig.h>
#include <unistd.h>
#include <string.h>
#include <time.h>
#include <sys/ioctl.h>
#include <net/if.h>
#include <errno.h>
#include <netinet/in.h>
#include <arpa/inet.h>

#include <libmnl/libmnl.h>
#include <linux/if.h>
#include <linux/if_link.h>
#include <linux/rtnetlink.h>

#include <linux/gtp.h>
#include <linux/if_link.h>

#include <libgtpnl/gtpnl.h>

#define INTERFACE_NAME_SIZE					10
#define FILE_NAME_SIZE				 		100
#define CMD_BUFFER_SIZE 					200
#define PGW_NUM_UE_POOL_MAX 					16
#define RETURNok						0
#define PGW_CONFIG_STRING_PGW_CONFIG                            "P-GW"
#define PGW_CONFIG_STRING_NETWORK_INTERFACES_CONFIG             "NETWORK_INTERFACES"
#define PGW_CONFIG_STRING_IP_ADDRESS_POOL                       "IP_ADDRESS_POOL"
#define PGW_CONFIG_STRING_IPV4_ADDRESS_LIST                     "IPV4_LIST"
#define PGW_CONFIG_STRING_PGW_MASQUERADE_SGI                    "PGW_MASQUERADE_SGI"
#define PGW_CONFIG_STRING_PGW_IPV4_ADDR_FOR_SGI                 "PGW_IPV4_ADDRESS_FOR_SGI"
#define PGW_CONFIG_STRING_PGW_INTERFACE_NAME_FOR_SGI            "PGW_INTERFACE_NAME_FOR_SGI"
#define PGW_CONFIG_STRING_IPV4_PREFIX_DELIMITER                 "/"
#define PGW_CONFIG_STRING_UE_MTU                                "UE_MTU"




typedef struct spgw_config_s {
	
	char      	 if_name_SGI[INTERFACE_NAME_SIZE];
	uint32_t         SGI;	  // read from system
        uint32_t         mtu_SGI; // read from system
        struct in_addr   addr_sgi;// read from system
        uint8_t          mask_sgi;// read from system
	bool             masquerade_SGI;
	int              num_ue_pool;
	uint8_t          ue_pool_mask[PGW_NUM_UE_POOL_MAX];
	struct in_addr   ue_pool_addr[PGW_NUM_UE_POOL_MAX];
	uint16_t  	 ue_mtu;
	
}spgw_config_e;

spgw_config_e config;

static void send_system_cmd(char *gtp_dev);
static int spgw_config_parse_file(char *Filename);
static char* mystrsep(char** stringp, const char* delim);

static char* mystrsep(char** stringp, const char* delim)
{
	char* start = *stringp;
	char* p;

	p = (start != NULL) ? strpbrk(start, delim) : NULL;

	if (p == NULL)
	{
		*stringp = NULL;
	}
	else
	{
		*p = '\0';
		*stringp = p + 1;
	}

	return start;
}

static int spgw_config_parse_file(char *Filename){
	
	config_t  cfg = {0};
	config_setting_t  *setting_pgw = NULL;
	config_setting_t  *subsetting = NULL;
	config_setting_t  *sub2setting = NULL;
	
	
	config.num_ue_pool = 0;
	
	int num = 0;
	char  *if_SGI = NULL;
	char  *masquerade_SGI = NULL;
	 char  *astring = NULL;
	char  *address = NULL;
	char  *mask    = NULL;
	char  *cidr    = NULL;
	unsigned char  buf_in_addr[sizeof (struct in_addr)];
	struct in_addr addr_start;
	int    mtu = 0;
	int   prefix_mask = 0;
	const char delimiters[] = PGW_CONFIG_STRING_IPV4_PREFIX_DELIMITER;
	
	config_init (&cfg);
	
	if (!config_read_file (&cfg, Filename)) {
      fprintf(stderr, "Failed to read %s:%d - %s\n", Filename, config_error_line (&cfg), config_error_text (&cfg));
      config_destroy (&cfg);
      exit(EXIT_FAILURE);
    }
	
	setting_pgw = config_lookup (&cfg, PGW_CONFIG_STRING_PGW_CONFIG);
	
	if (setting_pgw) {
		subsetting = config_setting_get_member (setting_pgw, PGW_CONFIG_STRING_NETWORK_INTERFACES_CONFIG);

		if (subsetting) {
		  if (config_setting_lookup_string (subsetting, PGW_CONFIG_STRING_PGW_INTERFACE_NAME_FOR_SGI, (const char **)&if_SGI)
			   && config_setting_lookup_string (subsetting, PGW_CONFIG_STRING_PGW_MASQUERADE_SGI, (const char **)&masquerade_SGI))
			 {
				strcpy(config.if_name_SGI, if_SGI);
				if (strcasecmp (masquerade_SGI, "yes") == 0) {
					config.masquerade_SGI = true;
				}
				else {
					config.masquerade_SGI = false;
				}
			}
			else {
				fprintf(stderr, "SPGW Network Interface configuration parsing failed");
		  }
		} 
		else {
		  fprintf(stderr, "network interface configuration not found in the spgw");
		}
	

		//!!!------------------------------------!!!
		subsetting = config_setting_get_member (setting_pgw, PGW_CONFIG_STRING_IP_ADDRESS_POOL);

		if (subsetting) {
		  sub2setting = config_setting_get_member (subsetting, PGW_CONFIG_STRING_IPV4_ADDRESS_LIST);
		  
		  if (sub2setting) {
			num = config_setting_length (sub2setting);

			for (int i = 0; i < num; i++) {
			  astring = config_setting_get_string_elem (sub2setting, i);

			  if (astring) {
				cidr = astring;
				address = mystrsep(&cidr, delimiters);
				mask = mystrsep(&cidr, delimiters);
				
				if (inet_pton (AF_INET, address, &addr_start) == 1) {
				  //memcpy (&addr_start, buf_in_addr, sizeof (struct in_addr));
				  // valid address
				  prefix_mask = atoi ((const char *)mask);

				  if ((prefix_mask >= 2) && (prefix_mask < 32) && (config.num_ue_pool < PGW_NUM_UE_POOL_MAX)) {
						//memcpy (&(config.ue_pool_addr[config.num_ue_pool]), buf_in_addr, sizeof (struct in_addr));
					config.ue_pool_addr[config.num_ue_pool] = addr_start;

					config.ue_pool_mask[config.num_ue_pool] = prefix_mask;
					config.num_ue_pool += 1;
				  } else {
					fprintf(stderr, "CONFIG POOL ADDR IPV4: BAD MASQ: %d\n", prefix_mask);
				  }
				}				
			  }
			}
		  } else {
			fprintf(stderr, "CONFIG POOL ADDR IPV4: NO IPV4 ADDRESS FOUND\n");
		  }
		}
			
		if (config_setting_lookup_int (setting_pgw, PGW_CONFIG_STRING_UE_MTU, &mtu)) {
		  config.ue_mtu = mtu;
		} else {
		  config.ue_mtu = 1463;
		}
	}
	
	config_destroy (&cfg);
	return RETURNok;
}

static void send_system_cmd(char *gtp_dev){

	//int gtp_dev_mtu = config.ue_mtu; 
	int rv = 0;
	
	char system_cmd[CMD_BUFFER_SIZE];

	 sprintf(system_cmd,"ip link set dev %s mtu %u", gtp_dev, config.ue_mtu);
  	 rv = system ((const char *)system_cmd);

	if(rv != 0){
		perror("IP link setting fails\n");
		exit(EXIT_FAILURE);
	}

	

  	struct in_addr ue_gw;
  	//ue_gw.s_addr = 0x10010ac;
	ue_gw.s_addr = config.ue_pool_addr[0].s_addr | htonl(1);

	int mask = config.ue_pool_mask[0];


  	sprintf (system_cmd,"ip addr add %s/%u dev %s", inet_ntoa(ue_gw), mask, gtp_dev);
  	rv = system ((const char *)system_cmd);
	if(rv != 0) {
		fprintf(stderr, "ip address set fails\n");
		exit(EXIT_FAILURE);
        }
	
	struct in_addr ue_net = config.ue_pool_addr[0];


	fprintf (stderr, "Setting route to reach UE net %s via %s\n", inet_ntoa(ue_net), gtp_dev);

	if (gtp_dev_config(gtp_dev, &ue_net, mask) < 0) {
	    fprintf (stderr,  "Cannot add route to reach network\n");
	    exit(EXIT_FAILURE);
	}

	sprintf (system_cmd, "sysctl -w net.ipv4.ip_forward=1");
	rv = system ((const char *)system_cmd);

	if(rv != 0){
		perror("IP forwarding fails\n");
		exit(EXIT_FAILURE);
	}
	


	sprintf (system_cmd, "sync");
	rv = system ((const char *)system_cmd);

	if(rv != 0){
		perror("sync fails\n");
		exit(EXIT_FAILURE);
	}


	sprintf (system_cmd,"iptables -t mangle -F FORWARD");
	rv = system ((const char *)system_cmd);
	if(rv != 0){
		perror("sync fails\n");
		exit(EXIT_FAILURE);
	}

	

	sprintf(system_cmd,"iptables -t nat -F POSTROUTING");
	rv = system ((const char *)system_cmd);
	if(rv != 0){
		perror("sync fails\n");
		exit(EXIT_FAILURE);
	}
	

    if(config.masquerade_SGI) { 
			 // GET SGi informations
	    struct ifreq ifr;
		char str_sgi[INET_ADDRSTRLEN];
	
	    memset(&ifr, 0, sizeof(ifr));
	    int fd = socket(AF_INET, SOCK_DGRAM, 0);
	    ifr.ifr_addr.sa_family = AF_INET;
	    strncpy(ifr.ifr_name, (const char *)config.if_name_SGI, IFNAMSIZ-1);
	    if (ioctl(fd, SIOCGIFADDR, &ifr)) {
	      fprintf (stderr, "Failed to read SGI ip addresses: error %s\n", strerror(errno));  
	    }
	    struct sockaddr_in* ipaddr = (struct sockaddr_in*)&ifr.ifr_addr;
	    if (inet_ntop(AF_INET, (const void *)&ipaddr->sin_addr, str_sgi, INET_ADDRSTRLEN) == NULL) {
	      fprintf (stderr, "inet_ntop");
	    }
	    config.SGI = ipaddr->sin_addr.s_addr;

	    ifr.ifr_addr.sa_family = AF_INET;
	    strncpy(ifr.ifr_name, (const char *)config.if_name_SGI, IFNAMSIZ-1);
	    if (ioctl(fd, SIOCGIFMTU, &ifr)) {
	      fprintf (stderr, "Failed to probe SGI MTU: error %s\n", strerror(errno));
	    }
	    config.mtu_SGI = ifr.ifr_mtu;    
	    close(fd);
	  	
		sprintf(system_cmd,"iptables -t nat -I POSTROUTING -s %s/%d -o %s  ! --protocol sctp -j SNAT --to-source %s",
			  inet_ntoa(ue_net), mask,
			  config.if_name_SGI, str_sgi);
		rv = system ((const char *)system_cmd); 
		fprintf(stderr,"system cmd %s\r\n",system_cmd);
		if(rv != 0){
			perror("sync fails\n");
			exit(EXIT_FAILURE);
		} 
	}
	
} 

int main(int argc, char *argv[])
{
	//char buf[MNL_SOCKET_BUFFER_SIZE];
	int ret, sgsn_mode = 0;
	char system_cmd[CMD_BUFFER_SIZE];


	if (argc < 3) {
		printf("Usage: %s <add|del> <device>\n", argv[0]);
		exit(EXIT_FAILURE);
	}

	if (!strcmp(argv[1], "del")) {
		printf("destroying gtp interface...\n");
		if (gtp_dev_destroy(argv[2]) < 0)
			perror("gtp_dev_destroy");

		return 0;
	}

	if (argc > 3 && !strcmp(argv[3], "--sgsn"))
		sgsn_mode = 1;
	
	sprintf (system_cmd,"sudo rmmod gtp");
	int rv = system ((const char *)system_cmd);
	sprintf (system_cmd,"sudo modprobe gtp");
	
	rv = system ((const char *)system_cmd);	
	if ( rv != 0) {
		fprintf(stderr,"Cannot change the kernel mode to gtp\n");
		exit(EXIT_FAILURE);
	}

	int fd1 = socket(AF_INET, SOCK_DGRAM, 0);
	int fd2 = socket(AF_INET, SOCK_DGRAM, 0);
	char *gtp_dev = argv[2];
	

	
	struct sockaddr_in sockaddr_fd1 = {
		.sin_family	= AF_INET,
		.sin_port	= htons(3386),
		.sin_addr	= {
			.s_addr 	= INADDR_ANY,
		},
	};
	struct sockaddr_in sockaddr_fd2 = {
		.sin_family	= AF_INET,
		.sin_port	= htons(2152),
		.sin_addr	= {
			.s_addr 	= INADDR_ANY,
		},
	};

	if (bind(fd1, (struct sockaddr *) &sockaddr_fd1,
		 sizeof(sockaddr_fd1)) < 0) {
		perror("bind");
		exit(EXIT_FAILURE);
	}
	if (bind(fd2, (struct sockaddr *) &sockaddr_fd2,
		 sizeof(sockaddr_fd2)) < 0) {
		perror("bind");
		exit(EXIT_FAILURE);
	}

	if (sgsn_mode)
		ret = gtp_dev_create_sgsn(-1, gtp_dev, fd1, fd2);
	else
		ret = gtp_dev_create(-1, gtp_dev, fd1, fd2);
	if (ret < 0) {
		perror("cannot create GTP device\n");
		exit(EXIT_FAILURE);
	}

	if(argc > 3 && !strcmp(argv[3],"cmd")){
		char  spgw_file[FILE_NAME_SIZE] = "/usr/local/etc/oai/spgw.conf";
		
		spgw_config_parse_file(spgw_file);
		send_system_cmd(gtp_dev);
	}

	/*fprintf(stderr, "WARNING: attaching dummy socket descriptors. Keep "
			"this process running.\n");*/
	


	while (1) {
		struct sockaddr_in addr;
		socklen_t len = sizeof(addr);
		sleep(1000);

		/*
		ret = recvfrom(fd1, buf, sizeof(buf), 0,
			       (struct sockaddr *)&addr, &len);
		printf("received %d bytes via UDP socket\n", ret);
		*/
	}

	return 0;
}

