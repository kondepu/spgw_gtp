#!/bin/bash

val=$(sudo ./gtp-tunnel list)
ver='v'$(echo $val | cut -d' ' -f 2)
tei=$(echo $val | cut -d' ' -f 4)
i_tei=$(echo $tei | cut -d'/' -f 1)
o_tei=$(echo $tei | cut -d'/' -f 2)
ms_addr=$(echo $val | cut -d' ' -f 6)
sgsn_addr=$(echo $val | cut -d' ' -f 8)
DevName="gtp0"
cmd=$(echo "add $DevName  $ver  $i_tei  $o_tei  $ms_addr  $sgsn_addr")
#cmd=$(echo " $ver  $i_tei  $o_tei  $ms_addr  $sgsn_addr")
echo $cmd